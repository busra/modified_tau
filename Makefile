.PHONY: run_main

run_main:
	Rscript run.R

clean:
	R --slave -q -e  "library(drake); clean();"
