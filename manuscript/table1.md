|      Gene profile      | Fagerberg Study | Uhlen Lab | GTEx Project | FANTOM5 Project | ENCODE Project |
|:----------------------:|:-----------------:|:-----------:|:------------:|:---------------:|:--------------:|
|     Null expression    |        1260       |     2427    |     2672     |       1808      |      3394   |
|     Weak expression    |        1808       |     2533    |     2788     |       3869      |      2976   |
| Wide-spread expression |       13126       |    11733    |    11434     |       8698      |     11013   |
|   Specific expression  |        2669       |     2983    |     2782     |       2063      |      2293   |
|   Total No. of genes   |       18863       |    19676    |    19676     |     16438       |     19676   |

Table: (\#tab:category) Number of genes in each category for all dataset.

