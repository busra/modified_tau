Table 2. Number of specific gene-tissue pairs based on tau and extended tau calculations.

|      Datasets     |    Tau   | Extended Tau |
|:-----------------:|:--------:|:------------:|
|  Fagerberg Study  |   2669   |     3370     |
|    Uhlen Lab      |   2983   |     4257     |
|    GTEx Project   |   2782   |     4680     |
|  FANTOM5 Project  |   2063   |     3982     |
|   ENCODE Project  |   2293   |     3097     |

