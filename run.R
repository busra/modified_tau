library(drake)

# required packages
require(future) # just for parallel make
require(tidyverse)
require(knitr)
require(purrr)
require(broom)
require(e1071)
#library(here)
source("code/functions.R")
options(scipen=99,digits=3)  # this does not affect write_csv, so use write_formatted_csv()

# if decided to separate functions into individual files
# f <- list.files("./functions", full.names = T) %>%
# map(source)

# the template plan,  will  be enumerated for any number of datasets
template_plan <- drake_plan(
  proteinCoding = read_tsv(file_in("data_processed/protein_coding.csv"),
                           col_types = cols()),   
  meta = read_tsv(file_in("data_processed/tissue_meta_sample__.txt"),
                      col_types = cols()), 
  file = "E-MTAB-sample__-query-results.txt",
  raw = read_tsv(file_in("data_raw/E-MTAB-sample__-query-results.txt"), 
                 col_types = cols(),
                 comment = "#"),
  data = preparefile(raw_sample__,
                     file_sample__, 
                     filter = proteinCoding, 
                     meta = meta_sample__ ),
  ftest_result = ftest_all(geneData),
  ftest_write = write_formatted_csv(ftest_result, file_out("tables/f_test_result.csv")),
  boxplot_alldata =  boxplot_all(geneData),
  boxplot_all_save = ggsave(boxplot_alldata, 
                            filename = file_out("figures/boxplot_all.png"),
                            width = 7, height = 7),
  boxplot_all_savepdf = ggsave(boxplot_alldata, 
                            filename = file_out("figures/boxplot_all.pdf"),
                            width = 7, height = 7),
  boxplot = boxplot_sample(data_sample__),
  boxplot_save = ggsave(boxplot_sample__,  
                        filename="figures/boxplot_sample__.png",
                        width = 7, height = 7),
  violinplot_alldata =  violinplot_all(geneData),
  violinplot_all_save = ggsave(violinplot_alldata, 
                               filename = file_out("figures/violinplot_all.png"),
                               width = 7, height = 7),
  violinplot_all_savepdf = ggsave(violinplot_alldata, 
                               filename = file_out("figures/violinplot_all.pdf"),
                               width = 7, height = 7),
  analysis_result = analysis_result(data_sample__),
  includedList = analysis_result_sample__ %>% 
                   dplyr::filter(Status=="Specific expression"),
  write_intermediate1 = write_formatted_csv(includedList_sample__,
                        file_out("data_processed/includedListsample__.csv")),
  clusterCenters = clusterCenters(includedList_sample__),
  combine = combine_values(includedList_sample__,
                           clusterCenters_sample__),
  threshold = get_threshold(combine_sample__),
  genetissuepair = get_genetissuepair(includedList_sample__,
                                      threshold_sample__,
                                      file_sample__),
  write_result = write_formatted_csv(genetissuepair_sample__, 
                                     file_out("results/geneTissuepairsample__.csv")),
strings_in_dots = "literals"
)

# plan for report Rmd files
reports <- drake_plan(
  report = rmarkdown::render(
    knitr_in("report__.Rmd"),
    output_file = file_out("reports/report__.html"),
    quiet = TRUE),
strings_in_dots = "literals"
)


data_list=c("1733","2836","3358","4344","5214")

report_list=c("01-import-clean-data",
              "02-data-distribution-Ftest",
              "03-tau-calculation", 
              "04-threshold-cluster-assign")

master_plan <- template_plan %>% 
  evaluate_plan(template_plan, rules = list(sample__ = data_list)) %>%
  gather_by(prefix = "geneData", filter = grepl("^data", target))

plan_all <- bind_plans(master_plan, reports) %>% 
  evaluate_plan(reports, rules = list(report__= report_list))

# project_config <- drake_config(plan_all)
# vis_drake_graph(project_config, targets_only = T)

future::plan(future::multiprocess) 
make(plan_all, parallelism = "future", jobs = 6)
