# Modified tau manuscript and analysis repo

In this repo we present our "modified tau" approach which can be used to identify genes exhibiting tissue specific expression. Our approach extends previously described tau calculation [Kryuchkova-Mostacci *et al.* 2017](https://academic.oup.com/bib/article/18/2/205/2562739) by matching a gene to **multiple tissues**.

**UPDATE** Our study is published in *BioData Mining* (https://doi.org/10.1186/s13040-022-00315-9)

# Re-runing the code

We used [`drake`](https://docs.ropensci.org/drake/) R package as workflow manager to analyze 5 different datasets. If you're interested in verifying and re-running the code, you can simply install `drake` package (and other necessary packages for analysis steps) and then run `make` in terminal. As an alternative, you can run the `drake.Rmd` file.

# Analysis and report files

We also included Rmarkdown files to describe the analysis steps. If you are interested in detailed examination of analysis step, you can follow along and run individual chunks. However, chunks access the intermadiate files calculated by `drake`. Thus, even if you are not interested in running the whole pipe, `drake` should run it at least once to generate intermediate files. 

# Resulting files

If you're interested in list of genes specific to tissues per sample please proceed to `results` folder. Both csv or Excel files are available for download.
